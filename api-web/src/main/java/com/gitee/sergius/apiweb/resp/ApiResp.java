package com.gitee.sergius.apiweb.resp;

import com.gitee.sergius.apitool.annotation.ApiModel;
import com.gitee.sergius.apitool.annotation.ApiModelProperty;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>响应封装参数</pre>
 *
 */
@ApiModel
public class ApiResp {

    @ApiModelProperty(desc = "用户主键" ,example = "1000" )
    private Integer age;

    @ApiModelProperty(name = "family-address",desc = "家庭住址" ,example = "北京朝阳区100号")
    private String address;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
