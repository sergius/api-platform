package com.gitee.sergius.apiweb.req;

import com.gitee.sergius.apitool.annotation.ApiModel;
import com.gitee.sergius.apitool.annotation.ApiModelProperty;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>请求封装参数</pre>
 *
 */
@ApiModel
public class ApiReq {

    @ApiModelProperty(name = "id",desc = "用户主键" ,example = "1000")
    private Integer id;

    @ApiModelProperty(desc = "用户名称",example = "张三")
    private String name;

    @ApiModelProperty(desc = "parent")
    private ApiReq parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ApiReq getParent() {
        return parent;
    }

    public void setParent(ApiReq parent) {
        this.parent = parent;
    }
}
