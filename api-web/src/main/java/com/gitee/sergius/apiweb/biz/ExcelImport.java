package com.gitee.sergius.apiweb.biz;

import com.gitee.sergius.apiweb.entity.School;
import com.gitee.sergius.exceltool.exception.ExcelException;
import com.gitee.sergius.exceltool.factory.ExcelToolFactory;
import com.gitee.sergius.exceltool.input.FieldProcessor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>描述：
 *
 * @author shawn yang
 * @version 1.00
 */
public class ExcelImport {


    public static void main(String[] args) throws FileNotFoundException, ExcelException {
        File file = new File("d:/import.xls");

        Map<String,Integer> reflectMap = new HashMap<>(5);
        reflectMap.put("name",0);
        reflectMap.put("createDate",1);
        reflectMap.put("address",2);
        reflectMap.put("type",3);
        reflectMap.put("feeLevel",4);

        Map<String,FieldProcessor> fieldProcessor = new HashMap<>(1);
        fieldProcessor.put("feeLevel", src -> {
            if("1000".equals(src)){
                return "一级";
            }else if("2000".equals(src)){
                return "二级";
            }else if("3000".equals(src)){
                return "三级";
            }else {
                return "";
            }
        });

        List<School> schools = ExcelToolFactory.createImportor()
                .dataFormat("yyyy/MM/dd HH:mm")
                .startRow(0)
                .entityClass(School.class)
                .inputFile(new FileInputStream(new File("d:/import.xls")))
                .reflectMap(reflectMap)
                .processMap(fieldProcessor)
                .produce();

        System.out.println(schools.size());



    }




}
