package com.gitee.sergius.apiweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>启动类</pre>
 *
 */
@SpringBootApplication
public class ApiWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiWebApplication.class, args);
    }
}
