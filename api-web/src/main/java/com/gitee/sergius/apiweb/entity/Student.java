package com.gitee.sergius.apiweb.entity;

import java.util.Date;

/**
 * <p>描述：
 *
 * @author shawn yang
 * @version 1.00
 */
public class Student {

    public Student(String name,int age,Date birth,int level){
        this.name = name;
        this.age = age;
        this.birth = birth;
        this.level = level;
    }

    private String name;
    private int age;
    private Date birth;

    /**
     * 学生年级
     *  1、一年级
     *  2、二年级
     *  3、三年级
     *  4、四年级
     *  5、五年级
     */
    private int level;

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
