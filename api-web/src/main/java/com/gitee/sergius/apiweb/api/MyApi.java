package com.gitee.sergius.apiweb.api;

import com.gitee.sergius.apitool.annotation.*;
import com.gitee.sergius.apitool.constant.DataType;
import com.gitee.sergius.apitool.constant.MediaType;
import com.gitee.sergius.apiweb.req.ApiReq;
import com.gitee.sergius.apiweb.resp.ApiResp;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>测试用接口</pre>
 *
 */
@RestController
@RequestMapping(value = "/api" , method = RequestMethod.GET)
@ApiExceptions({
        @ApiException(code = "200",desc = "请求成功"),
        @ApiException(code = "300",desc = "重定向失败"),
        @ApiException(code = "400",desc = "网络错误"),
        @ApiException(code = "500",desc = "socket错误")
})
@Api(contentType = MediaType.APPLICATION_JSON_UTF8)
public class MyApi {

    /**
     *
     * @param req
     * @param method
     * @return
     */
    @Api(desc = "app通过社区平台开通安防服务-form",tag = "3.3.1",contentType = MediaType.APPLICATION_FORM_URLENCODED)
    @RequestMapping(value = "/saveUser")
    public List<ApiResp> saveUser(@ApiParam(name="ApiReq", desc = "请求用户参数"  ) List<ApiReq> req,
                                  @ApiParam(name = "method",desc = "调用方法", required = false, example = "getUser") String method) throws IllegalAccessException, InstantiationException {
        System.out.println("id:"+req.get(0).getId()+",name:"+req.get(0).getName()+",method:"+method);

        ApiResp resp = new ApiResp();
        resp.setAge(20);
        resp.setAddress("山东省神和县");

        return null;
    }


    /**
     *
     * @param data
     * @return
     */
    @Api(desc = "app通过社区平台开通安防服务-json",tag = "3.3.1")
    @RequestMapping(value = "/getUserJson",method = {RequestMethod.POST})
    @ApiCustomerParams({
            @ApiCustomerParam(name = "email", desc = "用户邮箱" ,required = false,dataType = DataType.STRING ,example = "hello.world@163.com"),
            @ApiCustomerParam(name = "school", desc = "毕业院校" ,required = false,dataType = DataType.STRING , example = "中国大学"),
            @ApiCustomerParam(name = "name",desc = "用户名称",isArray = true, dataType = DataType.STRING),
            @ApiCustomerParam(name = "method",desc = "调用方法" ,required = false,dataType = DataType.STRING),
            @ApiCustomerParam(name = "user",desc = "用户系统信息",isArray = true,dataType = DataType.CLASS,clazz = com.gitee.sergius.apiweb.req.ApiReq.class)
    })
    public ApiResp getUserJson(@RequestBody Map<String, Object> data){
        System.out.println("email:"+data.get("email")+",school:"+data.get("school")+",name:"+data.get("name"));

        ApiResp resp = new ApiResp();
        resp.setAge(30);
        resp.setAddress("山东省沂源县");

        return resp;
    }


    /**
     *
     * @param apiReq
     * @return
     */
    @Api(desc = "app通过社区平台开通安防服务-json",tag = "3.3.2")
    @RequestMapping(value = "/getUserParamJson",method = {RequestMethod.GET,RequestMethod.POST})
    @ApiExceptions({
            @ApiException(code = "2000",desc = "请求成功"),
            @ApiException(code = "3000",desc = "重定向失败"),
            @ApiException(code = "4000",desc = "网络错误"),
            @ApiException(code = "5000",desc = "socket错误")
    })
    @ApiResponses({
            @ApiResponse(name = "name",desc = "用户姓名",dataType = DataType.STRING ,example = "李四"),
            @ApiResponse(name = "id",desc = "用户ID",dataType = DataType.LONG ,example = "1000"),
            @ApiResponse(name = "user",desc = "基本信息" ,dataType = DataType.CLASS ,isArray = true,clazz =ApiResp.class)
    })
    public Map<String,Object> getUserParamJson(
            @RequestBody @ApiParam(name = "apiReq" ,desc = "用户系统信息") ApiReq apiReq
                                    /*@RequestParam() @ApiParam(name = "email" ,desc = "用户邮箱" , example = "hell0.world@163.com") String email,
                                    @RequestParam() @ApiParam(name = "method" , desc = "调用方法",required = false,example = "saveUser") String method*/
            //@RequestBody Map<String,Object> param
    ){


        Map<String,Object> result = new HashMap<>(2);
        result.put("name","yang");
        result.put("id",1000);

        ApiResp resp = new ApiResp();
        resp.setAge(40);
        resp.setAddress("山东济南");

        result.put("user",resp);

        return result;
    }

}
