package com.gitee.sergius.apiweb.config;

import com.gitee.sergius.apitool.component.ApiComponent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>spring boot集成配置信息</pre>
 *
 */
@Configuration
@ComponentScan("com.gitee.sergius.apitool.web")
public class ApiWebConfiguration {
    @Bean
    public ApiComponent creatApiComponent(){
        return new ApiComponent()
                .withScanPackage("com.gitee.sergius.apiweb.api")
                .withYamlFilePath("D:/")
                .withParseDepth(3)
                .withServerHost("http://www.haiershequ.com/intelligent-community")
                .build();
    }
}
