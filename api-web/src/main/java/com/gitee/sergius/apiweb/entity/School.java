package com.gitee.sergius.apiweb.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>描述：
 *
 * @author shawn yang
 * @version 1.00
 */
public class School {

    private String name;

    private Date createDate;

    private String address;

    /**
     * 学校类型：1、小学 2、中学 3、大学
     */
    private int type;

    /**
     * 学费层级
     * 一级：1000
     * 二级：2000
     * 三级：3000
     */
    private String feeLevel;

    private List<Student> students = new ArrayList<>();

    public String getFeeLevel() {
        return feeLevel;
    }

    public void setFeeLevel(String feeLevel) {
        this.feeLevel = feeLevel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public School add(Student s){
        this.students.add(s);
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
