package com.gitee.sergius.apiweb.biz;

import com.gitee.sergius.apiweb.entity.School;
import com.gitee.sergius.apiweb.entity.Student;
import com.gitee.sergius.exceltool.exception.ExcelException;
import com.gitee.sergius.exceltool.factory.ExcelToolFactory;

import java.util.*;

import static com.gitee.sergius.exceltool.export.ExcelExporter.OTHER_REFLECT_KEY;

/**
 * <p>描述：
 *
 * @author shawn yang
 * @version 1.00
 */
public class ExcelExport {


    public static void main(String[] args) {
        List<School> schools = getSchools();
        simpleTest(schools);
        complexTest(schools);
    }



    public static List<School> getSchools() {
        List<School> schools = new ArrayList<>();

        School s1 = new School();
        s1.setName("青岛一中");
        s1.setType(1);
        s1.setAddress("青岛市崂山区中韩街道101号");
        s1.setCreateDate(new Date());
        s1.setFeeLevel("一级");

        Student st11 = new Student("张晓明", 10, new Date(), 1);
        Student st12 = new Student("黄章", 11, new Date(), 1);
        Student st13 = new Student("马冬梅", 13, new Date(), 4);

        s1.add(st11).add(st12).add(st13);
        schools.add(s1);

        School s2 = new School();
        s2.setName("中国海洋大学");
        s2.setType(4);
        s2.setAddress("青岛市崂山区松岭路238号");
        s2.setCreateDate(new Date());
        s2.setFeeLevel("三级");

        Student st21 = new Student("尹卓", 20, new Date(), 1);
        Student st22 = new Student("张成", 21, new Date(), 2);

        s2.add(st21).add(st22);
        schools.add(s2);
        return schools;
    }

    public static void simpleTest(List<School> schools) {
        try {
            ExcelToolFactory.createExportor().produce(schools).writeToFile("d:/", "example.xls");
        } catch (ExcelException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void complexTest(List<School> schools) {


        Map<String, HashMap<String, Object>> fieldReflectMap = new HashMap<>(3);
        HashMap<String, Object> switchMap = new HashMap<>(4);
        switchMap.put("1", "小学");
        switchMap.put("2", "中学");
        switchMap.put("3", "大学");
        switchMap.put(OTHER_REFLECT_KEY, "其他");
        fieldReflectMap.put("School.type", switchMap);


        HashMap<String, Object> switchMap2 = new HashMap<>(6);
        switchMap2.put("1", "一年级");
        switchMap2.put("2", "二年级");
        switchMap2.put("3", "三年级");
        switchMap2.put("4", "四年级");
        switchMap2.put("5", "五年级");
        switchMap2.put(OTHER_REFLECT_KEY, "其他");
        fieldReflectMap.put("Student.level", switchMap2);

        HashMap<String, Object> switchMap3 = new HashMap<>(4);
        switchMap3.put("一级", 1000);
        switchMap3.put("二级", 2000);
        switchMap3.put("三级", 3000);
        switchMap3.put(OTHER_REFLECT_KEY, "其他");
        fieldReflectMap.put("School.feeLevel", switchMap3);

        try {
            ExcelToolFactory.createExportor()
                    .columnWidth(40)
                    .dataFormat("yyyy/MM/dd")
                    .sheetTitle("学校列表")
                    .startColumn(2)
                    .startRow(4)
                    .headers("学校名称", "建校日期", "学校类型", "学费层级", "学生名字", "年龄", "年级")
                    .excludedFields("School.address", "Student.birth")
                    .fieldReflectMap(fieldReflectMap)
                    .produce(schools)
                    .writeToFile("d:/", "complex.xls");
        } catch (ExcelException e) {
            e.getMessage();
        }

    }
}
