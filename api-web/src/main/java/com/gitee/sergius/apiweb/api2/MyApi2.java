package com.gitee.sergius.apiweb.api2;

import com.gitee.sergius.apitool.annotation.*;
import com.gitee.sergius.apitool.constant.DataType;
import com.gitee.sergius.apitool.constant.MediaType;
import com.gitee.sergius.apiweb.req.ApiReq;
import com.gitee.sergius.apiweb.resp.ApiResp;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * <p>描述：
 *
 * @author shawn yang
 * @version 1.00
 */
@RestController
@RequestMapping(value = "/api" , method = RequestMethod.GET)
@ApiExceptions({
        @ApiException(code = "200",desc = "请求成功"),
        @ApiException(code = "300",desc = "重定向失败"),
        @ApiException(code = "400",desc = "网络错误"),
        @ApiException(code = "500",desc = "socket错误")
})
@Api(contentType = MediaType.APPLICATION_JSON_UTF8)
public class MyApi2 {

    /**
     * @return
     */
    @Api(desc = "app通过社区平台开通安防服务-json",tag = "3.3.1",contentType = MediaType.APPLICATION_FORM_URLENCODED)
    @RequestMapping(value = "/getUserJson2",method = {RequestMethod.POST})
    @ApiCustomerParam(name = "apiReq", desc = "入参" ,dataType = DataType.CLASS ,clazz = com.gitee.sergius.apiweb.req.ApiReq.class)

    public ApiResp getUserJson(ApiReq apiReq,String name){

        ApiResp resp = new ApiResp();
        resp.setAge(30);
        resp.setAddress("山东省沂源县");

        return resp;
    }
}
