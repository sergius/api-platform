# api-platform

#### 项目介绍
该项目提供常用的一些工具，项目中的api-web是测试用的包，主要用来对其他项目做测试，其他各项目功能有：
1. api-generator是自动生成项目api文档的工具。
2. excel-processer是处理excel导入/导出的工具。

---
### 一、api-generator

#### 安装教程

spring-boot集成：
1. 添加依赖
    - 添加依赖包
        ```xml
        <dependency>
            <groupId>com.gitee.sergius</groupId>
            <artifactId>api-generator</artifactId>
            <version>2.0.0</version>
        </dependency>
        ```

2. 添加配置bean：
    ```java
    @Bean
    public ApiComponent creatApiComponent(){
        return new ApiComponent()
                .withScanPackage("com.gitee.sergius.smarthome.intelligent.depot.restapi")
                .withYamlFilePath("D:/")
                .withParseDepth(3)
                .withServerHost("http://www.haiershequ.com/intelligent-community")
                .build();
    }
    ```

- addScanPackage：添加api扫描包，可以一次添加多个包，例如 addScanPackage（"com.package1","com.package2"）
- withYamlFilePath:配置扫描完成后是否生成存储文件，该文件存储各个api信息，如果配置了该路径，当接口方法上面的方法tag变更之后，会保留之前的接口信息，这样做主要是为了可以做接口版本控制，如果不需要，可以不配置此项，此时如果修改了接口tag，之前的版本将不做保留。
- withParseDepth：请求参数和返回参数递归扫描的层数，主要用来防止当请求参数或者响应参数配置中如果有循环嵌套的时候出现死循环。
- withServerHost：此参数主要用来配置页面显示的接口请求路径的主机地址部分。
3. 添加扫描的controller包：
    ```java
    @ComponentScan("com.gitee.sergius.apitool.web")
    ```
spring-mvc集成：
1. 添加依赖
    - 添加依赖包
        ```xml
        <dependency>
            <groupId>com.gitee.sergius</groupId>
            <artifactId>api-generator</artifactId>
            <version>2.0.0</version>
        </dependency>
        ```
2. 添加配置bean：
    ```java
   @Configuration
   @ComponentScan("com.gitee.sergius.apitool.web")
   public class ApiConfiguration {

        @Bean
        public ApiComponent apiComponent(){
            return new ApiComponent()
                .withScanPackage("com.sandalice.api.controller")
                .withServerHost("http://haiershequ.com:7531/IntelligentCommunity")
                .withYamlFilePath("d:/")
                .withYamlFileName("doc.yaml")
                .build();
        }
    }
    ```
  >也可以在spring mvc的配置文件中通过配置项<context:component-scan base-package="com.gitee.sergius.apitool.*"/>添加扫描controller。

3.添加servlet-mapping
    为保证/api-doc接口请求url可以被分发，需要确认分发类DispatcherServlet能够处理/api-doc请求，该项通常在web.xml中配置，举例：
  ```xml
    <servlet>
    <servlet-name>sandalice</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    <init-param>
      <param-name>contextConfigLocation</param-name>
      <param-value></param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
  </servlet>
  <servlet-mapping>
    <url-pattern>*.htm</url-pattern>
    <url-pattern>*.json</url-pattern>
    <url-pattern>*.jsp</url-pattern>
    <url-pattern>/api-doc</url-pattern>
  </servlet-mapping>

  ```

     如上配置项中的url-pattern

---
#### 注解说明

1. @Api 
    ```java
        @Api(desc = "接口说明",contentType = MediaType.APPLICATION_FORM_URLENCODED,tag = "1.0")
    ```
    - desc：接口说明
    - contentType ：接口请求方式
    - tag：接口标签
>该注解可以用在**类|方法**上面，类上可以不添加，但是需要生成接口文档的方法上面必须添加，`desc`仅用在接口方法上时才有效，`contentType`和`tag`优先采用接口方法上的定义。
2. @ApiCustomerParams、@ApiCustomerParam 两个需要配合使用
    多个入参时：
    ```java
        @ApiCustomerParams({
            @ApiCustomerParam(name = "email", desc = "用户邮箱" ,required = false,dataType = DataType.STRING ,example = "hello.world@163.com"),
            @ApiCustomerParam(name = "school", desc = "毕业院校" ,required = false,dataType = DataType.STRING , example = "中国大学"),
            @ApiCustomerParam(name = "name",desc = "用户名称",isArray = true, dataType = DataType.STRING),
            @ApiCustomerParam(name = "method",desc = "调用方法" ,required = false,dataType = DataType.STRING),
            @ApiCustomerParam(name = "user",desc = "用户系统信息",isArray = true,dataType = DataType.CLASS,clazz = com.gitee.sergius.apitool.req.ApiReq.class)
    })
    ```
    单个入参时可简写：
    ```java
      @ApiCustomerParam(name = "user",desc = "用户系统信息",isArray = true,dataType = DataType.CLASS,clazz = com.gitee.sergius.apitool.req.ApiReq.class)
    ```
    - name：字段名称 
    - desc：字段说明
    - required：是否必填（默认为true）
    - dataType：字段数据类型
    - example：示例
    - isArray: 是否是集合类型（默认为false）
    - clazz：实体类型对应的类路径，只有当dataType=DataType.CLASS时才必填
>该注解可以用在**方法**上面，当入参不能通过方法参数直接判断出来时使用。
3. @ApiParam 
    ```java
        @ApiParam(name = "userId" , desc = "用户Id",required = false,example = "1000")
    ```
    - name：字段名称 
    - desc：字段说明
    - required：是否必填（默认为true）
    - example：示例
>该注解可以用在**方法参数**上面，当没有定义`@ApiCustomerParams`和`@ApiCustomerParam`时使用，用于当入参可以直接通过方法参数判断出来的时候使用。
4. @ApiResponses、@ApiResponse 两个需要配合使用
    多个返回参数时
    ```java
    @ApiResponses({
            @ApiResponse(name = "name",desc = "用户姓名",dataType = DataType.STRING ,example = "李四"),
            @ApiResponse(name = "id",desc = "用户ID",dataType = DataType.LONG ,example = "1000"),
            @ApiResponse(name = "user",desc = "基本信息" ,dataType = DataType.CLASS ,isArray = true,clazz =com.gitee.sergius.apitool.resp.ApiResp.class)
    })
    ```
    单个返回参数时可简写：
    ```java
    @ApiResponse(name = "user",desc = "基本信息" ,dataType = DataType.CLASS ,isArray = true,clazz =com.gitee.sergius.apitool.resp.ApiResp.class)
    ```
    - name：字段名称 
    - desc：字段说明
    - required：是否必填（默认为true）
    - dataType：字段数据类型
    - example：示例
    - isArray: 是否是集合类型（默认为false）
    - clazz：实体类型对应的类路径，只有当dataType=DataType.CLASS时才必填
>该注解可以用在**方法**上面，当出参不能通过方法返回类型直接判断出来时使用。
 5. @ApiExceptions、@ApiException 两个需要配合使用
    多个返回参数时
    ```java
    @ApiExceptions({
            @ApiException(code = "200",desc = "请求成功"),
            @ApiException(code = "300",desc = "重定向失败"),
            @ApiException(code = "400",desc = "网络错误"),
            @ApiException(code = "500",desc = "socket错误")
    })
    ```
    单个返回参数时可简写：
    ```java
    @ApiException(code = "200",desc = "请求成功")
    ```
    - code：响应码 
    - desc：响应说明
>该注释可以用在**类|方法**上面，优先采用方法上的定义。
6. @ApiModel
    ```java
    @ApiModel
    ```
>该注解用在**类**上面，不含属性，当入参或者出参为`CLASS`封装类型时，该注解用来定义对应的封装类。
7. @ApiModelProperty
    ```java
    @ApiModelProperty(name = "id",desc = "用户主键" ,example = "1000")
    ```
    - name：字段名称 
    - desc：字段说明
    - required：是否必填（默认为true）
    - example：示例
>该注解用在**属性**上，当入参或者出参为`CLASS`封装类型时，该注解用来定义对应的封装类的各个属性。

---
### 请求地址
http(s)://${host}/api-doc   

---
### 注解配置表示例
可以参考：`api-platform/api-web/src/main/java/com/gitee/sergius/apiweb/api/MyApi.java`

---
### 生成API文档界面展示
![API展示界面](https://images.gitee.com/uploads/images/2018/0813/172115_e4b3150a_1867545.jpeg "output.jpg")

### 二、excel-processer

#### 安装教程
1. 添加依赖
    - 添加依赖包
        ```xml
        <dependency>
            <groupId>com.gitee.sergius</groupId>
            <artifactId>excel-processer</artifactId>
            <version>1.0.0</version>
        </dependency>
        ```
        
#### 使用说明

1. 导入功能：（目前仅支持.xls文件的导入,示例参考：`api-platform/api-web/src/main/java/com/gitee/sergius/apiweb/biz/ExcelImport.java`）
    ```java
        List<School> schools = ExcelToolFactory.createImportor()
            .dataFormat("yyyy/MM/dd HH:mm")
            .startRow(0)
            .entityClass(School.class)
            .inputFile(new FileInputStream(new File("d:/import.xls")))
            .reflectMap(reflectMap)
            .processMap(fieldProcessor)
            .produce();
    ```
    - dataFormat：用来定义excel文件中日期格式类型数据的格式样式。默认为“yyyy-MM-dd HH:mm:ss”
    - startRow: 标识从excel文件中的哪一行开始导入，从0开始，默认为1
    - entityClass：配置要将excel文件中每行数据转换成的实体对象类
    - inputFile：配置数据读入来源，接受InputStream对象
    - reflectMap：配置实体对象域和excel列的对应关系，举例：
        ```java
        Map<String,Integer> reflectMap = new HashMap<>(5);
        reflectMap.put("name",0);
        reflectMap.put("createDate",1);
        reflectMap.put("address",2);
        reflectMap.put("type",3);
        reflectMap.put("feeLevel",4);
        ```
        该例表示将第0列数据保存到实体的`name`域中,第1列保存到`createDate`域中，。。。
    - processMap：配置某一列的数据处理，用在某一列需要做转换的情况下，举例：
        ```java
         Map<String,FieldProcessor> fieldProcessor = new HashMap<>(1);
                 fieldProcessor.put("feeLevel", src -> {
                     if("1000".equals(src)){
                         return "一级";
                     }else if("2000".equals(src)){
                         return "二级";
                     }else if("3000".equals(src)){
                         return "三级";
                     }else {
                         return "";
                     }
                 });
        ```
         该例表示feeLevel域在存储时，如果excel中值为1000，则对应存储为`一级`,如果为2000，存储为`二级`,...
    - produce：此方法要在最后调用，用来按照上面的配置项将excel转换成对应的实体对象列表。
2. 导出功能：（目前仅支持.xls文件的导出,示例参考：`api-platform/api-web/src/main/java/com/gitee/sergius/apiweb/biz/ExcelExport.java`）
    ```java
        try {
                    ExcelToolFactory.createExportor()
                            .columnWidth(40)
                            .dataFormat("yyyy/MM/dd")
                            .sheetTitle("学校列表")
                            .startColumn(2)
                            .startRow(4)
                            .headers("学校名称", "建校日期", "学校类型", "学费层级", "学生名字", "年龄", "年级")
                            .excludedFields("School.address", "Student.birth")
                            .fieldReflectMap(fieldReflectMap)
                            .produce(schools)
                            .writeToFile("d:/", "complex.xls");
                } catch (ExcelException e) {
                    e.getMessage();
                }
    ```
    - columnWidth：设置列宽，最小5，最大500，不设置默认为15
    - dataFormat：设置日期类型数据展示格式，不设置默认为“yyyy-MM-dd HH:mm:ss”
    - sheetTitle：设置sheet页名称，不设置默认为sheet1
    - startColumn：设置数据写入起始列，从0开始，默认为0
    - startRow: 设置数据写入起始行，从0开始，默认为0
    - headers：设置列名称
    - excludedFields：设置不导出的域，形式为“类名.属性名”，例如：User.bizType，需要导出的字段必须要有get方法否则抛出NoSuchMethodException
    - fieldReflectMap：可以用该参数对数据库导出的某列数据做转换，举例：
        ```java
                Map<String, HashMap<String, Object>> fieldReflectMap = new HashMap<>(3);
                HashMap<String, Object> switchMap = new HashMap<>(4);
                switchMap.put("1", "小学");
                switchMap.put("2", "中学");
                switchMap.put("3", "大学");
                switchMap.put(OTHER_REFLECT_KEY, "其他");
                fieldReflectMap.put("School.type", switchMap);
        ```
        该例表示将School对象中type域的值做转换，如果值为1，导出到excel中展示为“小学”，。。。
        如果有不需要转换的值，可以用`switchMap.put(OTHER_REFLECT_KEY, "其他");`表示除1、2、3之外的值将被转换成“其他”
    - produce：按照上面的配置生成excel对象的实体缓存，该方法要在以上所有配置项之后调用。
    - writeToFile、write、writeToHttpResponse：此方法要在最后调用，用来将生成的excel对象缓存写到对应的输出流中。其中：
        writeToFile：将excel写到文件中。入参为对应的excel文件的存储路径和名称。
        writeToHttpResponse：将excel写到HttpServletResponse中，用来web项目中页面下载。入参为对应的HttpServletResponse和生成的文件名。
        write：将excel缓存写到对应的输出流中，入参为OutputStream。