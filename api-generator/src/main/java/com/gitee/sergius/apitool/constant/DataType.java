package com.gitee.sergius.apitool.constant;

/**
 * 描述：定义前端展示数据类型.
 *
 * @author shawn yang
 * @version 1.00
 */
public enum DataType {

    /**
     * Integer类型，包含类型为int、byte、short、Integer、Byte、Short类型，默认展示为：{@code 0}
     */
    INTEGER("Integer","0",false),

    /**
     * Long类型，包含Long和long类型，默认展示为：{@code 1000}
     */
    LONG("Long","1000",false),

    /**
     * Float类型，包含Float和float类型，默认展示为：{@code 0.01}
     */
    FLOAT("Float","0.01",false),

    /**
     *Double类型，包含Double和double类型，默认展示为：{@code 1000.001}
     */
    DOUBLE("Double","1000.001",false),

    /**
     * Boolean类型，包含Boolean和boolean类型，默认展示为：{@code true}
     */
    BOOLEAN("Boolean","true",false),

    /**
     * Date类型，默认展示为：{@code "2010-01-01 10:10:00"}
     */
    DATE("Date","2010-01-01 10:10:00",true),

    /**
     * String类型，包含String、Character、char，默认展示为：{@code "string"}
     */
    STRING("String","string",true),

    /**
     * Map类型，包含Map及其子类
     */
    MAP("Map","{}",false),

    /**
     * Class类型，封装类型
     */
    CLASS("Object","",false),

    /**
     * 其他做映射的类型，默认展示为空
     */
    OTHER("","",false);

    String name;
    String defaultHtml;
    boolean withQuotation;

    DataType(String name,String defaultHtml,boolean withQuotation){
        this.name = name;
        this.defaultHtml = defaultHtml;
        this.withQuotation = withQuotation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDefaultHtml() {
        return defaultHtml;
    }

    public void setDefaultHtml(String defaultHtml) {
        this.defaultHtml = defaultHtml;
    }

    public boolean isWithQuotation() {
        return withQuotation;
    }

    public void setWithQuotation(boolean withQuotation) {
        this.withQuotation = withQuotation;
    }
}
