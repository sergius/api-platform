package com.gitee.sergius.apitool.constant;

import com.gitee.sergius.apitool.entity.YamlEntity;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>描述：公共静态变量
 *
 * @author shawn yang
 * @version 1.00
 */
public class Constant {
    /**
     * 参数的解析深度，默认为3
     */
    public static int PARSE_DEPTH = 3;

    /**
     * 接口访问主机地址
     */
    public static String API_HOST = "http://ip:port/";

    /**
     * 解析接口生成的中间缓存Map
     */
    public static Map<String,YamlEntity> yamlEntityMap = new LinkedHashMap<>();

    /**
     * 返回类型未指定字段名时默认的名称
     */
    public static final String DEFAULT_UNDEFINE_NAME = "default-undefine.novalue";

    /**
     * 返回类型未指定字段名时默认的描述
     */
    public static final String DEFAULT_UNDEFINE_DESC = "";

    /**
     * 返回类型未指定字段名时默认的必填字段
     */
    public static final boolean DEFAULT_UNDEFINE_REQUIRED = false;

    /**
     * 返回类型未指定字段名时默认的示例
     */
    public static final String DEFAULT_UNDEFINE_EXAMPLE = "";
}
