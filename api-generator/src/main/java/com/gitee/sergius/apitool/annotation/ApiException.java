package com.gitee.sergius.apitool.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>该注释可以用在类|方法上面，优先采用方法上的定义，用来定义接口响应码。
 * 可以单独使用，也可以结合{@link ApiExceptions}一起使用</pre>
 */
@Target({TYPE,METHOD})
@Retention(RUNTIME)
@Documented
public @interface ApiException {
    /**
     * 响应码
     */
    String code() ;

    /**
     * 响应码说明，描述该响应码具体含义
     */
    String desc();

}
