package com.gitee.sergius.apitool.annotation;

import com.gitee.sergius.apitool.constant.DataType;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>该注解可以用在方法<b>参数</b>上面，当没有定义{@link ApiCustomerParams}和{@link ApiCustomerParam}时使用，用于当请求参数可以直接通过方法参数判断出来的时候使用。</pre>
 *
 */
@Target(PARAMETER)
@Retention(RUNTIME)
@Documented
public @interface ApiParam {
    /**
     * 参数名
     */
    String name() ;

    /**
     * 参数说明，描述参数具体含义
     */
    String desc() default "";

    /**
     * 是否为必填字段
     */
    boolean required() default true;

    /**
     * 参数示例，如果不设置，将采用{@link DataType} 中配置的默认值
     */
    String example() default "";
}
