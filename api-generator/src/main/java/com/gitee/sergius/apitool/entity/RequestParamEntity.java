package com.gitee.sergius.apitool.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>请求参数解析之后存储的实体</pre>
 *
 */
public class RequestParamEntity{
    private String name;
    private String type;
    private boolean required;
    private String desc;
    private boolean hasChild = false;
    private boolean isArray = false;
    private String example;
    private int level;
    private List<RequestParamEntity> childList = new ArrayList<>();

    /**
     * 获取参数层级
     */
    public int getLevel() {
        return level;
    }

    /**
     *设置参数层级
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * 获取参数样例
     */
    public String getExample() {
        return example;
    }

    /**
     * 设置参数样例
     */
    public void setExample(String example) {
        this.example = example;
    }

    /**
     * 判断是否时数组类型
     */
    public boolean isArray() {
        return isArray;
    }

    /**
     * 设置是否是数组类型
     */
    public void setArray(boolean array) {
        isArray = array;
    }

    /**
     * 获取参数名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置参数名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取参数类型
     */
    public String getType() {
        return type;
    }

    /**
     * 设置参数类型
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取是否是必填项
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * 设置是否是必填项
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * 获取参数描述
     */
    public String getDesc() {
        return desc;
    }

    /**
     * 设置参数描述
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * 判断是否有子层级
     */
    public boolean isHasChild() {
        return hasChild;
    }

    /**
     * 设置是否有子层级
     */
    public void setHasChild(boolean hasChild) {
        this.hasChild = hasChild;
    }

    /**
     * 获取子层级
     */
    public List<RequestParamEntity> getChildList() {
        return childList;
    }

    /**
     * 设置子层级
     * @param childList
     */
    public void setChildList(List<RequestParamEntity> childList) {
        this.childList = childList;
    }
}
