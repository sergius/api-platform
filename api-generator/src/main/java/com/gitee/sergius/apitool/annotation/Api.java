package com.gitee.sergius.apitool.annotation;


import com.gitee.sergius.apitool.constant.MediaType;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>该注解可以用在<b>类|方法</b>上面，类上可以不添加，但是需要生成接口文档的方法上面必须添加，desc仅用在接口方法上时才有效，contentType和tag优先采用接口方法上的定义。</pre>
 *
 */
@Target({TYPE,METHOD})
@Retention(RUNTIME)
@Documented
public @interface Api {
    /**
     * 接口说明，描述接口用途
     */
    String desc() default "";

    /**
     * 接口标签，对接口进行分组管理
     */
    String tag() default "";

    /**
     * 接口请求数据类型
     * @see MediaType
     */
    MediaType contentType() default MediaType.APPLICATION_JSON_UTF8;
}
