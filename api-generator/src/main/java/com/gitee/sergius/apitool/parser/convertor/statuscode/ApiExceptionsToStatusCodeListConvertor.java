package com.gitee.sergius.apitool.parser.convertor.statuscode;

import com.gitee.sergius.apitool.annotation.ApiException;
import com.gitee.sergius.apitool.annotation.ApiExceptions;
import com.gitee.sergius.apitool.entity.StatusCodeEntity;

import java.util.List;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>用于解析{@link ApiExceptions}和{@link ApiException}标签定义的响应码</pre>
 *
 */
public class ApiExceptionsToStatusCodeListConvertor {
    /**
     * 将{@link ApiExceptions}和{@link ApiException}定义的响应码转换成对应的实体类
     * @param apiExceptionArr {@link ApiException}定义的响应码数据
     * @param statusCodeList 用来保存解析出来的响应码数据
     */
    public static void parse(ApiException[] apiExceptionArr , List<StatusCodeEntity> statusCodeList){

        if(apiExceptionArr != null && apiExceptionArr.length > 0){
            for(ApiException apiException : apiExceptionArr){
                StatusCodeEntity statusCodeEntity = new StatusCodeEntity();
                statusCodeEntity.setCode(apiException.code());
                statusCodeEntity.setDesc(apiException.desc());
                statusCodeList.add(statusCodeEntity);
            }
        }

    }
}
