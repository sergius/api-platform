package com.gitee.sergius.apitool.parser.convertor.response;

import com.gitee.sergius.apitool.annotation.ApiResponse;
import com.gitee.sergius.apitool.annotation.ApiResponses;
import com.gitee.sergius.apitool.entity.ResponseParamEntity;
import com.gitee.sergius.apitool.annotation.ApiModel;

import java.lang.reflect.Method;
import java.util.List;

import static com.gitee.sergius.apitool.constant.Constant.*;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>当接口没有定义{@link ApiResponses}和{@link ApiResponse}时，将根据方法返回类型解析</pre>
 *
 */
public class ReturnTypeResponseToEntityConvertor {



    /**
     * 根据方法的返回类型来生成响应存储实体
     * @param method  接口方法
     * @param responseParamEntityList 要存储解析数据的实体
     */
    public static void parse(Method method,List<ResponseParamEntity> responseParamEntityList){
        Class returnClazz = method.getReturnType();
        if(returnClazz == void.class){
            return;
        }

        if (returnClazz.isAnnotationPresent(ApiModel.class)) {
            responseParamEntityList.addAll(ResponseModelToEntityConvertor.parseModelToEntity(returnClazz,1));
        } else {

            ResponseParamEntity responseParamEntity = ResponseNotModelToEntityConvertor.parseNotModelToEntity(returnClazz,method.getGenericReturnType(),
                    DEFAULT_UNDEFINE_NAME,DEFAULT_UNDEFINE_DESC,DEFAULT_UNDEFINE_REQUIRED,DEFAULT_UNDEFINE_EXAMPLE,1);

            responseParamEntityList.add(responseParamEntity);
        }
    }
}
