package com.gitee.sergius.apitool.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>该注释可以用在类|方法上面，优先采用方法上的定义，用来定义接口响应码。
 * 需结合{@link ApiException}一起使用</pre>
 */
@Target({TYPE,METHOD})
@Retention(RUNTIME)
@Documented
public @interface ApiExceptions {
    ApiException[] value();
}
