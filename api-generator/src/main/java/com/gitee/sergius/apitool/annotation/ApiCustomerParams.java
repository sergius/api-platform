package com.gitee.sergius.apitool.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>该注解可以用在<b>方法</b>上面，当入参不能通过方法参数直接判断出来时使用。
 * 需要结合{@link ApiCustomerParam}一起使用</pre>
 */
@Target(METHOD)
@Retention(RUNTIME)
@Documented
public @interface ApiCustomerParams {
    ApiCustomerParam[] value();
}
