package com.gitee.sergius.apitool.parser.convertor.response;

import com.gitee.sergius.apitool.annotation.ApiModel;
import com.gitee.sergius.apitool.constant.DataType;
import com.gitee.sergius.apitool.entity.ResponseParamEntity;
import com.gitee.sergius.apitool.tool.TypeReflectUtil;

import java.lang.reflect.Type;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>用于解析非封装类型数据定义的响应参数</pre>
 *
 */
public class ResponseNotModelToEntityConvertor {

    /**
     * 非自定义封装转换成实体
     * @param paramClazz 代表该类型数据的class
     * @param type 如果是集合类（List及其子类、Set及其子类、数组类型），此参数代表其中的泛型类型
     * @param name 参数名
     * @param desc 参数描述
     * @param required 参数是否必填
     * @param example 参数示例
     * @param level 参数所处层级
     * @return ResponseParamEntity 解析后生成的响应参数存储实体
     */
    public static ResponseParamEntity parseNotModelToEntity(Class paramClazz, Type type, String name, String desc, boolean required, String example, int level) {

        ResponseParamEntity responseParamEntity = new ResponseParamEntity();
        responseParamEntity.setName(name);
        responseParamEntity.setDesc(desc);
        responseParamEntity.setRequired(required);
        responseParamEntity.setExample(example);
        responseParamEntity.setLevel(level);

        if (TypeReflectUtil.paramClazzIsList(paramClazz)) {
            responseParamEntity.setArray(true);
            Class genericType = TypeReflectUtil.parseGenericType(paramClazz, type);
            if (genericType.isAnnotationPresent(ApiModel.class)) {
                responseParamEntity.setType(DataType.CLASS.getName());
                responseParamEntity.setHasChild(true);
                responseParamEntity.setChildList(ResponseModelToEntityConvertor.parseModelToEntity(genericType,level+1));
            } else {
                //此处做简单处理，默认list里面除了封装类型，只有基本类型，不包含其他集合类型
                responseParamEntity.setHasChild(false);
                responseParamEntity.setType(TypeReflectUtil.getEntityTypeByClass(paramClazz));
            }
        } else if (TypeReflectUtil.paramClazzIsMap(paramClazz)) {
            //map不做处理
            responseParamEntity.setArray(false);
            responseParamEntity.setHasChild(false);
            responseParamEntity.setType(DataType.MAP.getName());
        } else {//其他基本类型
            responseParamEntity.setArray(false);
            responseParamEntity.setHasChild(false);
            responseParamEntity.setType(TypeReflectUtil.getEntityTypeByClass(paramClazz));
        }

        return responseParamEntity;
    }
}
