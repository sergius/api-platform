package com.gitee.sergius.apitool.entity;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>状态码解析之后存储的实体</pre>
 *
 */
public class StatusCodeEntity {
    private String code;
    private String desc;

    /**
     * 获取响应码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置响应码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取响应码描述
     */
    public String getDesc() {
        return desc;
    }

    /**
     * 设置响应码描述
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }
}
