package com.gitee.sergius.apitool.constant;

/**
 * 描述：定义数据请求方式
 *
 * @author shawn yang
 * @version 1.00
 */
public enum MediaType {
    /**
     *form表单形式请求，content-type=application/x-www-form-urlencoded
     */
    APPLICATION_FORM_URLENCODED("application/x-www-form-urlencoded"),

    /**
     * json形式请求，content-type=application/json
     */
    APPLICATION_JSON("application/json"),

    /**
     * json形式请求，编码格式为UTF-8，content-type=application/json;charset=UTF-8
     */
    APPLICATION_JSON_UTF8("application/json;charset=UTF-8");

    String value;

    MediaType(String value){
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }






}
