package com.gitee.sergius.apitool.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>用于将解析生成的中间文件存储到yaml文件</pre>
 *
 */
public class YamlFileExtend {

    private List<YamlEntity> anno = new ArrayList<>();

    /**
     * 获取所有标签分组
     */
    public List<YamlEntity> getAnno() {
        return anno;
    }

    /**
     * 设置标签分组数据
     * @param anno
     */
    public void setAnno(List<YamlEntity> anno) {
        this.anno = anno;
    }
}
