package com.gitee.sergius.apitool.entity;

import com.gitee.sergius.apitool.constant.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>用于存储每个接口信息的实体</pre>
 *
 */
public class SubEntity{


    private String desc ;
    private Long generateTime;
    private String method;
    private String url;
    private String reqContentType;
    private List<RequestParamEntity> requestParamEntityList = new ArrayList<>();
    private List<ResponseParamEntity> responseParamEntityList = new ArrayList<>();
    private List<StatusCodeEntity> statusCodeEntityList = new ArrayList<>();

    /**
     * 获取最近一次接口解析时间
     */
    public Long getGenerateTime() {
        return generateTime;
    }

    /**
     * 设置接口解析时间
     * @param generateTime
     */
    public void setGenerateTime(Long generateTime) {
        this.generateTime = generateTime;
    }

    /**
     * 获取接口描述
     */
    public String getDesc() {
        return desc;
    }

    /**
     * 设置接口描述
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * 获取请求方式，{@link org.springframework.web.bind.annotation.RequestMethod}
     */
    public String getMethod() {
        return method;
    }

    /**
     * 设置接口请求方式
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * 获取接口请求url，RequestMapping#value
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置接口请求url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 获取请求content-type，MediaType#name
     */
    public String getReqContentType() {
        return reqContentType;
    }

    /**
     * 设置请求content-type
     */
    public void setReqContentType(String reqContentType) {
        this.reqContentType = reqContentType;
    }

    /**
     * 获取请求参数解析之后的实体数据
     */
    public List<RequestParamEntity> getRequestParamEntityList() {
        return requestParamEntityList;
    }

    /**
     * 设置请求参数解析之后的实体数据
     */
    public void setRequestParamEntityList(List<RequestParamEntity> requestParamEntityList) {
        this.requestParamEntityList = requestParamEntityList;
    }

    /**
     * 获取响应参数解析之后的实体数据
     */
    public List<ResponseParamEntity> getResponseParamEntityList() {
        return responseParamEntityList;
    }

    /**
     * 设置响应参数解析之后的实体数据
     */
    public void setResponseParamEntityList(List<ResponseParamEntity> responseParamEntityList) {
        this.responseParamEntityList = responseParamEntityList;
    }

    /**
     * 获取响应码解析之后的实体数据
     */
    public List<StatusCodeEntity> getStatusCodeEntityList() {
        return statusCodeEntityList;
    }

    /**
     * 设置响应码解析之后的实体数据
     */
    public void setStatusCodeEntityList(List<StatusCodeEntity> statusCodeEntityList) {
        this.statusCodeEntityList = statusCodeEntityList;
    }
}
