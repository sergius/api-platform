package com.gitee.sergius.apitool.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>用于存储每个分组标签信息的实体</pre>
 *
 */
public class YamlEntity {

    private String tag;

    private List<SubEntity> subEntityList = new ArrayList<>();

    /**
     * 获取标签名称
     */
    public String getTag() {
        return tag;
    }

    /**
     * 设置标签名称
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * 获取该分组标签下的接口信息
     */
    public List<SubEntity> getSubEntityList() {
        return subEntityList;
    }

    /**
     * 设置该分组标签下的接口信息
     * @param subEntityList
     */
    public void setSubEntityList(List<SubEntity> subEntityList) {
        this.subEntityList = subEntityList;
    }
}
