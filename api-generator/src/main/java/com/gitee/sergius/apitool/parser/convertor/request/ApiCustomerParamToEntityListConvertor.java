package com.gitee.sergius.apitool.parser.convertor.request;

import com.gitee.sergius.apitool.annotation.ApiCustomerParam;
import com.gitee.sergius.apitool.annotation.ApiCustomerParams;
import com.gitee.sergius.apitool.constant.DataType;
import com.gitee.sergius.apitool.entity.RequestParamEntity;

import java.util.List;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>用于解析{@link ApiCustomerParams}和{@link ApiCustomerParam}标签定义的请求参数</pre>
 *
 */
public class ApiCustomerParamToEntityListConvertor {

    /**
     * 将方法上添加的{@link ApiCustomerParams}和{@link ApiCustomerParam}注解解析成实体类
     *
     * @param apiCustomerParamArr {@link ApiCustomerParam}注解定义的入参数组
     * @param requestParamEntityList 用来保存解析出来的入参数据
     */
    public static void parse(ApiCustomerParam[] apiCustomerParamArr, List<RequestParamEntity> requestParamEntityList) {

        if (apiCustomerParamArr == null) {
            return;
        }

        for (ApiCustomerParam apiCustomerParam : apiCustomerParamArr) {
            if (apiCustomerParam.dataType() == DataType.CLASS) {

                classTypeToEntity(apiCustomerParam, requestParamEntityList);

            } else {

                notClassTypeToEntity(apiCustomerParam, requestParamEntityList);

            }
        }

    }

    private static void notClassTypeToEntity(ApiCustomerParam apiCustomerParam, List<RequestParamEntity> requestParamEntityList) {

        RequestParamEntity requestParamEntity = new RequestParamEntity();
        requestParamEntity.setName(apiCustomerParam.name());
        requestParamEntity.setDesc(apiCustomerParam.desc());
        requestParamEntity.setRequired(apiCustomerParam.required());
        requestParamEntity.setExample(apiCustomerParam.example());
        requestParamEntity.setType(apiCustomerParam.dataType().getName());
        requestParamEntity.setArray(apiCustomerParam.isArray());
        requestParamEntity.setHasChild(false);
        requestParamEntity.setLevel(1);

        requestParamEntityList.add(requestParamEntity);

    }

    private static void classTypeToEntity(ApiCustomerParam apiCustomerParam, List<RequestParamEntity> requestParamEntityList) {

        Class customerModelClass = apiCustomerParam.clazz();

        List<RequestParamEntity> customerList = RequestModelToEntityConvertor.parseModelToEntity(customerModelClass, 1);

        requestParamEntityList.addAll(customerList);

    }
}
