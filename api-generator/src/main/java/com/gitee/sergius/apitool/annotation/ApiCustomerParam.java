package com.gitee.sergius.apitool.annotation;

import com.gitee.sergius.apitool.constant.DataType;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>该注解可以用在<b>方法</b>上面，当入参不能通过方法参数直接判断出来时使用。
 * 可以单独使用，也可以结合{@link ApiCustomerParams}一起使用</pre>
 */
@Target(METHOD)
@Retention(RUNTIME)
@Documented
public @interface ApiCustomerParam {
    /**
     * 参数名
     */
    String name() ;

    /**
     * 参数说明，描述参数具体含义
     */
    String desc() default "";

    /**
     * 是否为必填字段
     */
    boolean required() default true;

    /**
     * 参数类型.
     * 如果是{@link DataType#CLASS} , clazz()必填.
     * @see DataType
     */
    DataType dataType();

    /**
     * 当{@link #dataType()}为DataType#CLASS时，该字段必填，表示要解析参数的类
     */
    Class clazz() default Object.class;

    /**
     * 如果数据类型是{@link java.util.Set}（包含子类）、{@link java.util.List}（包含子类）、数组，此项需设置为{@code true}
     */
    boolean isArray() default false;

    /**
     * 参数示例，如果不设置，将采用{@link DataType} 中配置的默认值
     */
    String example() default "";
}
