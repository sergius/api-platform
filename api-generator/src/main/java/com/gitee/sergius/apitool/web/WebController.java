package com.gitee.sergius.apitool.web;

import com.gitee.sergius.apitool.component.ApiComponent;
import com.gitee.sergius.apitool.constant.Constant;
import com.gitee.sergius.apitool.entity.YamlEntity;
import com.gitee.sergius.apitool.web.html.HtmlOperater;
import com.gitee.sergius.apitool.entity.SubEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>API页面展示路由控制</pre>
 *
 */
@Controller
public class WebController {

    @Autowired
    private ApiComponent apiComponent;

    @RequestMapping("/api-doc")
    public void spiDoc(HttpServletRequest request, HttpServletResponse response){

        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        PrintWriter writer = null;
        try{
            writer = response.getWriter();
            writer.println( "<!DOCTYPE html>" );
            writer.println(" <html> ");
            writer.println(" <head> ");
            writer.println(" <meta charset='UTF-8'> ");
            writer.println(" <title>接口文档</title> ");
            writer.println(" <link rel='stylesheet' href='https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css'> ");
            writer.println(" <script src='https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js'></script> ");
            writer.println(" <script src='https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js'></script> ");
            writer.println(HtmlOperater.getStylePart());
            writer.println(" </head> ");
            writer.println(" <body> ");

            Map<String,YamlEntity> pamlEntityMap = Constant.yamlEntityMap;
            if(CollectionUtils.isEmpty(pamlEntityMap)){
                writer.println(" <h1>No Data !</h1> ");
            }else{

                Iterator<String> tags = pamlEntityMap.keySet().iterator();
                while(tags.hasNext()){
                    String tag = tags.next();
                    YamlEntity yamlEntity = pamlEntityMap.get(tag);

                    writer.println(" <h3><span class='tag-span glyphicon glyphicon-tag'></span><span>"+tag+"</span></h3><hr> ");

                    List<SubEntity> subEntityList = yamlEntity.getSubEntityList();
                    if(!CollectionUtils.isEmpty(subEntityList)){
                        for(SubEntity subEntity : subEntityList){
                            writer.println(HtmlOperater.getBodyPart(subEntity));
                        }
                    }

                    writer.println(" <hr> ");
                }

            }

            writer.println(" </body> ");
            writer.println(HtmlOperater.getScriptPart());
            writer.println(" </html> ");
        }catch(IOException e){
            e.printStackTrace();
        }finally {
            if(writer != null){
                writer.close();
            }
        }

    }
}
