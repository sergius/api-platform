package com.gitee.sergius.apitool.parser;

import com.gitee.sergius.apitool.annotation.ApiException;
import com.gitee.sergius.apitool.constant.MediaType;
import com.gitee.sergius.apitool.entity.StatusCodeEntity;
import com.gitee.sergius.apitool.annotation.Api;
import com.gitee.sergius.apitool.annotation.ApiExceptions;
import com.gitee.sergius.apitool.parser.convertor.statuscode.ApiExceptionsToStatusCodeListConvertor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>用于解析类上添加的注解</pre>
 *
 */
public class ClassAnnotationParser {

    /**
     * 解析出类上定义的请求content-type，Api#contentType
     * @param apiClass  要解析的类
     * @param mediaType 用来存储解析出来的请求content-type
     */
    public static void parseClassMediaType(Class<?> apiClass , MediaType mediaType){
        if(apiClass.isAnnotationPresent(Api.class)){
            mediaType = apiClass.getAnnotation(Api.class).contentType();
        }
    }


    /**
     * 解析出类上定义的接口标签，Api#tag
     * @param apiClass 要解析的类.
     * @return 分组标签名称，若未定义，返回""
     */
    public static String  parseClassTag(Class<?> apiClass){
        if(apiClass.isAnnotationPresent(Api.class)){
            return apiClass.getAnnotation(Api.class).tag();
        }

        return "";
    }



    /**
     * 解析类上面添加的响应码注解，{@link ApiExceptions}|{@link ApiException}
     * @param apiClass 要解析的类.
     * @param parentStatusCodeList 用来存储解析出来的数据的实体列表
     */
    public static void parseClassStatusCodeList(Class<?> apiClass,List<StatusCodeEntity> parentStatusCodeList){
        ApiException[] apiExceptionArr = null;
        if(apiClass.isAnnotationPresent(ApiExceptions.class)){
            apiExceptionArr = apiClass.getAnnotation(ApiExceptions.class).value();
        }else if(apiClass.isAnnotationPresent(ApiException.class)){
            apiExceptionArr = new ApiException[]{apiClass.getAnnotation(ApiException.class)};
        }
        if(apiExceptionArr != null && apiExceptionArr.length > 0){
            ApiExceptionsToStatusCodeListConvertor.parse(apiExceptionArr,parentStatusCodeList);
        }
    }

    /**
     * 解析出类上面添加的RequestMapping#value注解定义的请求url地址
     * @param apiClass 要解析的类
     * @param urlList 用来存储解析出来的url列表
     */
    public static void parseClassUrlList(Class<?> apiClass,List<String> urlList){

        if(urlList == null){
            urlList = new ArrayList<>();
        }

        if(apiClass.isAnnotationPresent(RequestMapping.class)){
            String[] requestMappingValus = apiClass.getAnnotation(RequestMapping.class).value();
            if(requestMappingValus != null){
                for(String value : requestMappingValus){
                    urlList.add(value);
                }
            }
        }

    }

    /**
     * 解析出类上面添加的RequestMapping#method注解定义的请求方式
     * @param apiClass 要解析的类
     * @param requestMethodList 用来存储解析出来的{@link RequestMethod}列表
     */
    public static void parseClassRequestMethod(Class<?> apiClass,List<RequestMethod> requestMethodList){

        if(requestMethodList == null){
            requestMethodList = new ArrayList<>();
        }

        if(apiClass.isAnnotationPresent(RequestMapping.class)){
            RequestMethod[] requestMappingValus = apiClass.getAnnotation(RequestMapping.class).method();
            if(requestMappingValus != null){
                for(RequestMethod method : requestMappingValus){
                    requestMethodList.add(method);
                }
            }
        }

        if(requestMethodList.size() == 0){
            requestMethodList.add(RequestMethod.GET);
            requestMethodList.add(RequestMethod.POST);
        }

    }

}
