package com.gitee.sergius.apitool.parser.convertor.response;

import com.gitee.sergius.apitool.annotation.ApiResponse;
import com.gitee.sergius.apitool.annotation.ApiResponses;
import com.gitee.sergius.apitool.constant.DataType;
import com.gitee.sergius.apitool.entity.ResponseParamEntity;

import java.util.List;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>用于解析{@link ApiResponses}和{@link ApiResponse}标签定义的响应参数</pre>
 */
public class ApiResponseToEntityListConvertor {

    /**
     * 将方法上添加的{@link ApiResponses}和{@link ApiResponse}注释解析成实体类
     *
     * @param apiResponsesArr         {@link ApiResponse}定义的响应参数数据
     * @param responseParamEntityList 用来保存解析出来的出参数据
     */
    public static void parse(ApiResponse[] apiResponsesArr, List<ResponseParamEntity> responseParamEntityList) {

        if (apiResponsesArr == null) {
            return;
        }

        for (ApiResponse apiResponse : apiResponsesArr) {
            if (apiResponse.dataType() == DataType.CLASS) {
                int paramCount = 1;
                if(apiResponsesArr.length > 1){
                    paramCount = 2;
                }

                classTypeToEntity(apiResponse, responseParamEntityList,paramCount);

            } else {

                notClassTypeToEntity(apiResponse, responseParamEntityList);

            }
        }

    }

    private static void notClassTypeToEntity(ApiResponse apiResponse, List<ResponseParamEntity> responseParamEntityList) {

        ResponseParamEntity responseParamEntity = new ResponseParamEntity();
        responseParamEntity.setName(apiResponse.name());
        responseParamEntity.setDesc(apiResponse.desc());
        responseParamEntity.setRequired(apiResponse.required());
        responseParamEntity.setExample(apiResponse.example());
        responseParamEntity.setType(apiResponse.dataType().getName());
        responseParamEntity.setArray(apiResponse.isArray());
        responseParamEntity.setHasChild(false);
        responseParamEntity.setLevel(1);

        responseParamEntityList.add(responseParamEntity);

    }

    /**
     * 由ApiResponse定义的注解数量，如果只有一个，并且是对象类型，将直接将对象里面的属性解耦出来，否则，外层包一层说明
     * @param apiResponse
     * @param responseParamEntityList
     * @param paramCount
     */
    private static void classTypeToEntity(ApiResponse apiResponse, List<ResponseParamEntity> responseParamEntityList,int paramCount) {

        Class responseModelClass = apiResponse.clazz();

        if(paramCount == 1){
            List<ResponseParamEntity> customerList = ResponseModelToEntityConvertor.parseModelToEntity(responseModelClass, 1);
            responseParamEntityList.addAll(customerList);
        }else{
            List<ResponseParamEntity> customerList = ResponseModelToEntityConvertor.parseModelToEntity(responseModelClass, 2);

            ResponseParamEntity responseParamEntity = new ResponseParamEntity();
            responseParamEntity.setName(apiResponse.name());
            responseParamEntity.setDesc(apiResponse.desc());
            responseParamEntity.setRequired(apiResponse.required());
            responseParamEntity.setType(apiResponse.dataType().getName());
            responseParamEntity.setArray(apiResponse.isArray());
            responseParamEntity.setHasChild(true);
            responseParamEntity.setChildList(customerList);
            responseParamEntity.setLevel(1);

            responseParamEntityList.add(responseParamEntity);
        }

    }

}
