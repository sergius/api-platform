package com.gitee.sergius.apitool.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>该注解用在类上面，不含属性，当入参或者出参为封装类型时，该注解用来定义对应的封装类。</pre>
 *
 */
@Target(TYPE)
@Retention(RUNTIME)
@Documented
public @interface ApiModel {
}
