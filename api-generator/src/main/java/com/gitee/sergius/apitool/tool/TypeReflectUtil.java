package com.gitee.sergius.apitool.tool;


import com.gitee.sergius.apitool.constant.DataType;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>映射工具类</pre>
 *
 */
public class TypeReflectUtil {


    /**
     * 将代表数据类型的clazz转换成页面上展示的数据类型
     * @param paramClazz 代表数据类型的class
     * @return 页面展示类型，所有可用的数据类型{@link DataType}
     */
    public static String getEntityTypeByClass(Class paramClazz){

        DataType frontType = DataType.OTHER;

        if(paramClazz == int.class || paramClazz == byte.class || paramClazz == short.class ||
                paramClazz == Integer.class || paramClazz == Byte.class || paramClazz == Short.class){
            frontType = DataType.INTEGER;
        }else if(paramClazz == long.class || paramClazz == Long.class){
            frontType = DataType.LONG;
        }else if(paramClazz == float.class || paramClazz == Float.class ){
            frontType = DataType.FLOAT;
        }else if(paramClazz == double.class || paramClazz == Double.class){
            frontType = DataType.DOUBLE;
        }else if(paramClazz == boolean.class || paramClazz == Boolean.class){
            frontType = DataType.BOOLEAN;
        }else if(paramClazz == char.class || paramClazz == Character.class ||
                paramClazz == String.class){
            frontType = DataType.STRING;
        }else if(paramClazz == Date.class){
            frontType = DataType.DATE;
        }

        return frontType.getName();
    }

    /**
     * 判断某一个Class是否是Map（包含子类）类型
     * @param clazz 要判断的Class
     */
    public static boolean paramClazzIsMap(Class clazz){
        if(clazz.isAssignableFrom(Map.class)){
            return true;
        }else{
            Class[] interfaces = clazz.getInterfaces();
            if(interfaces != null){
                for(Class face : interfaces) {
                    if(face == Map.class){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 判断某一个Class是否是List（包含子类）、Set（包含子类）、数组 类型
     * @param clazz  要判断的Class
     */
    public static boolean paramClazzIsList(Class clazz){
        if (clazz.isAssignableFrom(List.class) || clazz.isAssignableFrom(Set.class) || clazz.isArray()){
            return true;
        }else{
            Class[] interfaces = clazz.getInterfaces();
            if(interfaces != null){
                for(Class face : interfaces) {
                    if(face == List.class || face == Set.class){
                        return true;
                    }
                }
            }
        }
        return false;
    }


    /**
     * 获取集合类型（Map除外）包含的泛型类型
     * @param clazz  要解析的Class
     * @param type 集合上的泛型类型
     */
    public static Class parseGenericType(Class clazz,Type type ){

        if(!paramClazzIsList(clazz)){
            return null;
        }
        if(clazz.isArray()){
            return clazz.getComponentType();
        }else{

            if(type instanceof ParameterizedType)
            {
                ParameterizedType pt = (ParameterizedType) type;

                return (Class)pt.getActualTypeArguments()[0];

            }
        }

        return null;
    }



}
