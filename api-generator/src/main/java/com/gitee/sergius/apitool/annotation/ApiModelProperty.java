package com.gitee.sergius.apitool.annotation;

import com.gitee.sergius.apitool.constant.DataType;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author shawn yang
 * @version 1.0.0
 * <p><pre>该注解用在<b>属性</b>上，当入参或者出参为封装类型时，该注解用来定义对应的封装类的各个属性。</pre>
 *
 */
@Target(FIELD)
@Retention(RUNTIME)
@Documented
public @interface ApiModelProperty {

    /**
     * 参数名，如果不指定该值，将跟属性名一致
     */
    String name() default "";

    /**
     * 参数描述，描述参数具体含义
     */
    String desc() default "";

    /**
     * 是否为必填字段
     */
    boolean required() default true;

    /**
     * 参数示例，如果不设置，将采用{@link DataType} 中配置的默认值
     */
    String example() default "";
}
