package com.gitee.sergius.exceltool.factory;

import com.gitee.sergius.exceltool.export.ExcelExporter;
import com.gitee.sergius.exceltool.input.ExcelImporter;

/**
 * <p>描述：excel 处理工具类统一创建工厂
 *
 * @author shawn yang
 * @version 1.00
 */
public class ExcelToolFactory {

    /**
     * 创建导出工具
     * @return
     */
    public static ExcelExporter createExportor(){
        return new ExcelExporter();
    }

    /**
     * 创建导入工具
     * @return
     */
    public static ExcelImporter createImportor(){
        return new ExcelImporter();
    }

}
