package com.gitee.sergius.exceltool.exception;

/**
 * <p>描述：excel处理自定义异常类
 *
 * @author shawn yang
 * @version 1.00
 */
public class ExcelException extends Exception{

    public ExcelException(String message){
        super(message);
    }

}
