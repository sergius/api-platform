package com.gitee.sergius.exceltool.input;

/**
 * <p>描述：
 *
 * @author shawn yang
 * @version 1.00
 */
public interface FieldProcessor {

    String process(String src);

}
